from .team import Team

class League():
    def __init__(self):
        self.current_matchday = 1
        self.team_pool = {}

    def get_team(self, team):
        if not self.team_pool.get(team, None):
            if self.current_matchday == 1:
                self.team_pool[team] = Team(team)
            else:
                return None, "Didn't recognize \"" + team + "\" as a team in the current league"
        return self.team_pool[team], None

    def apply_scores(self, team_1, score_1, team_2, score_2):
        if score_1 > score_2:
            team_1.add_win(self.current_matchday)
            team_2.add_loss(self.current_matchday)
        elif score_1 < score_2:
            team_1.add_loss(self.current_matchday)
            team_2.add_win(self.current_matchday)
        else:
            team_1.add_tie(self.current_matchday)
            team_2.add_tie(self.current_matchday)

    def is_new_day(self, t1, t2):
        return (self.current_matchday == t1.last_played or
                self.current_matchday == t2.last_played)

    @staticmethod
    def format_score(team):
        result = team.name + ", " + str(team.score) + " pt"
        if team.score != 1:
            result += "s"
        return result

    def daily_summary(self):
        title = "Matchday " + str(self.current_matchday)
        top_three = sorted(self.team_pool.values(), key=lambda t: (-t.score, t.name))[:3]
        if top_three == []:
            return title
        results = "\n".join(map(self.format_score, top_three))
        return title + "\n" + results + "\n"

    def process_match(self, team_1, score_1, team_2, score_2):
        t1,e1 = self.get_team(team_1)
        s1 = int(score_1)
        t2,e2 = self.get_team(team_2)
        s2 = int(score_2)

        errors = list(filter(lambda x: x, [e1, e2]))
        if len(errors) > 0:
            return None, errors

        summary = None
        if self.is_new_day(t1, t2):
            summary = self.daily_summary()
            self.current_matchday += 1
        self.apply_scores(t1, s1, t2, s2)

        return summary, errors

