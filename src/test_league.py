import unittest
from .league import League

class TestLeague(unittest.TestCase):
    def test_init(self):
        sut = League()
        self.assertEqual(sut.current_matchday, 1)
        self.assertEqual(len(sut.team_pool), 0)

    def test_get_team(self):
        sut = League()
        name = "Smitty Werbenjaegermanjensen"
        t,e = sut.get_team(name) # first get creates the team in the pool
        self.assertIsNone(e)
        self.assertEqual(sut.current_matchday, 1)
        self.assertEqual(len(sut.team_pool), 1)
        self.assertEqual(t.name, name)

        t2,e2 = sut.get_team(name) # second get retrieves existing
        self.assertIsNone(e2)
        self.assertIs(t, t2)

    def test_get_team_does_not_exist(self):
        sut = League()
        sut.current_matchday = 2 # no new teams after day one
        name = "Richard Simmons, et al."
        t, e = sut.get_team(name)
        self.assertIsNone(t)
        self.assertIsNotNone(e)

    def test_apply_scores_t2_win(self):
        sut = League()
        t1 = sut.get_team("Every animal you've eaten since the first grade")[0]
        t2 = sut.get_team("Argo loves Alice Coltrane")[0]
        sut.current_matchday = 100
        sut.apply_scores(t1, 15, t2, 5000)
        self.assertEqual(t1.score, 0)
        self.assertEqual(t1.last_played, 100)
        self.assertEqual(t2.score, 3)
        self.assertEqual(t2.last_played, 100)

    def test_apply_scores_t1_win(self):
        sut = League()
        t1 = sut.get_team("Calbee Shrimp Chips")[0]
        t2 = sut.get_team("Snakes love the augmented second")[0]
        sut.current_matchday = 100000
        sut.apply_scores(t1, 15000, t2, 5000)
        self.assertEqual(t1.score, 3)
        self.assertEqual(t1.last_played, 100000)
        self.assertEqual(t2.score, 0)
        self.assertEqual(t2.last_played, 100000)

    def test_apply_scores_tie(self):
        sut = League()
        t1 = sut.get_team("Calbee Shrimp Chips")[0]
        t2 = sut.get_team("Snakes love the augmented second")[0]
        sut.current_matchday = 100000000
        sut.apply_scores(t1, 15000, t2, 15000)
        self.assertEqual(t1.score, 1)
        self.assertEqual(t1.last_played, 100000000)
        self.assertEqual(t2.score, 1)
        self.assertEqual(t2.last_played, 100000000)

    def test_is_new_day(self):
        sut = League()
        t1 = sut.get_team("Calbee Shrimp Chips")[0]
        t2 = sut.get_team("Snakes love the augmented second")[0]
        self.assertFalse(sut.is_new_day(t1, t2))

        sut.current_matchday = 40
        t1.last_played = 40

        self.assertTrue(sut.is_new_day(t1, t2))

    def test_daily_summary(self):
        sut = League()
        self.assertEqual(sut.daily_summary(), "Matchday 1")

        t1 = sut.get_team("Snoring Dogs")[0]
        t2 = sut.get_team("Skittering Squirrels")[0]
        t3 = sut.get_team("African Violets")[0]
        t4 = sut.get_team("Zurich Zoomers")[0] # they never win and always get truncated
        sut.current_matchday = 17
        t1.add_win(17)
        t2.add_tie(17)
        t3.add_loss(17)
        self.assertEqual(
                sut.daily_summary(),
                "Matchday 17\nSnoring Dogs, 3 pts\nSkittering Squirrels, 1 pt\nAfrican Violets, 0 pts\n")

    def test_daily_summary_same_scores(self):
        sut = League()
        self.assertEqual(sut.daily_summary(), "Matchday 1")

        sut.get_team("Snoring Dogs")[0]
        sut.get_team("Skittering Squirrels")[0]
        sut.get_team("African Violets")[0]
        sut.get_team("Zurich Zoomers")[0] # they never win and always get truncated
        sut.current_matchday = 17
        self.assertEqual(
                sut.daily_summary(),
                "Matchday 17\nAfrican Violets, 0 pts\nSkittering Squirrels, 0 pts\nSnoring Dogs, 0 pts\n")
