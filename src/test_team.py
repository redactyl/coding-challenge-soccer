import unittest
from .team import Team

class TestTeam(unittest.TestCase):
    def test_init_and_add_win(self):
        name = "Bavarian Brawlers"
        sut = Team(name)
        self.assertEqual(sut.name, name)
        self.assertEqual(sut.score, 0)
        self.assertEqual(sut.last_played, 0)

        sut.add_win(15)
        self.assertEqual(sut.score, 3)
        self.assertEqual(sut.last_played, 15)

    def test_add_tie(self):
        sut = Team("A Pile of Angry Raccoons")
        sut.add_tie(42)
        self.assertEqual(sut.score, 1)
        self.assertEqual(sut.last_played, 42)

    def test_add_loss(self):
        sut = Team("Pizza the Hutt")
        sut.add_loss(3)
        self.assertEqual(sut.score, 0)
        self.assertEqual(sut.last_played, 3)
