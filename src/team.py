class Team():
    def __init__(self, name, lp=0):
        self.name = name
        self.score = 0
        self.last_played = lp

    def add_win(self, matchday):
        self.score += 3
        self.last_played = matchday

    def add_tie(self, matchday):
        self.score += 1
        self.last_played = matchday

    def add_loss(self, matchday):
        self.last_played = matchday
