import fileinput
import re
import sys
from .league import League

def main():
    pattern = re.compile(r"(.+) (\d+), (.+) (\d+)\n")
    league = League()
    with fileinput.input() as f:
        for line in f:
            match = pattern.fullmatch(line)
            if not match:
                print("COULDN'T MATCH LINE: " + line, file=sys.stderr)
                continue
            team_1, score_1, team_2, score_2 = match.groups()
            summary, errors = league.process_match(team_1, score_1, team_2, score_2)
            if summary:
                print(summary)
            if len(errors) > 0:
                print("\n".join(errors), file=sys.stderr)
    print(league.daily_summary())

if __name__ == '__main__':
    main()
